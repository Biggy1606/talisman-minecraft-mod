package TalismanMod;

import net.minecraft.item.ItemGroup;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.event.lifecycle.FMLClientSetupEvent;
import net.minecraftforge.fml.event.lifecycle.FMLCommonSetupEvent;
import net.minecraftforge.fml.javafmlmod.FMLJavaModLoadingContext;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

@Mod("talisman")
public class TalismanMod {
    public static TalismanMod instance;
    public static final String MODID = "talisman";

    public static final ItemGroup CREATIVETABNAME = new CreativeTab();

    public static final Logger LOGGER = LogManager.getLogger(MODID);
    private static ResourceLocation location(String name) {
        return new ResourceLocation(MODID, name);
    }

    public TalismanMod() {
        instance = this;
        FMLJavaModLoadingContext.get().getModEventBus().addListener(this::setup);
        FMLJavaModLoadingContext.get().getModEventBus().addListener(this::clientRegistries);
        MinecraftForge.EVENT_BUS.register(this);
    }

    private void setup(final FMLCommonSetupEvent event) {
        LOGGER.info("Setup method registered");
    }

    private void clientRegistries(final FMLClientSetupEvent event) {
        LOGGER.info("Client method registered");
    }
}
