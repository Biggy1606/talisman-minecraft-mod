package TalismanMod;

import TalismanMod.common.blocks.BlockList;
import TalismanMod.common.items.ItemList;
import TalismanMod.common.items.ItemToolBase;
import TalismanMod.common.procedures.RunicSwordHitEffect;
import net.minecraft.block.AbstractBlock;
import net.minecraft.block.Block;
import net.minecraft.block.SoundType;
import net.minecraft.block.material.Material;
import net.minecraft.entity.LivingEntity;
import net.minecraft.item.*;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.common.ToolType;
import net.minecraftforge.event.RegistryEvent;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.common.Mod;

import java.util.HashMap;
import java.util.Map;

@Mod.EventBusSubscriber(bus = Mod.EventBusSubscriber.Bus.MOD)
public class RegistryEvents {
    private static ResourceLocation location(String name) {
        return new ResourceLocation(TalismanMod.MODID, name);
    }

    @SubscribeEvent
    public static void registerItems(final RegistryEvent.Register<Item> event) {
        event.getRegistry().registerAll
                (
                        //Items
                        ItemList.coin = new Item(new Item.Properties().tab(TalismanMod.CREATIVETABNAME)).setRegistryName(location("coin")),
                        //Blocks
                        ItemList.tutorial_block = new BlockItem(BlockList.tutorial_block, new Item.Properties().tab(TalismanMod.CREATIVETABNAME)).setRegistryName(BlockList.tutorial_block.getRegistryName()),
                        //Weapons
                        ItemList.runic_sword = new SwordItem(ItemToolBase.runicMaterial, 0, -2.4f, new Item.Properties().tab(TalismanMod.CREATIVETABNAME)) {
                            @Override
                            public boolean hurtEnemy(ItemStack itemstack, LivingEntity entity, LivingEntity sourceEntity) {
                                boolean retrieval = super.hurtEnemy(itemstack, entity, sourceEntity);
                                {
                                    if (entity.getHealth() <= 0.0f) {
                                        Map<String, Object> $_dependencies = new HashMap<>();
                                        $_dependencies.put("sourceEntity", sourceEntity);
                                        RunicSwordHitEffect.executeProcedure($_dependencies);
                                    }
                                }
                                return retrieval;
                            }
                        }.setRegistryName(location("runic_sword")),
                        ItemList.sword = new SwordItem(ItemToolBase.swordMaterial, -1, -2.4f,new Item.Properties().tab(TalismanMod.CREATIVETABNAME)).setRegistryName(location("sword")),
                        ItemList.large_sword = new SwordItem(ItemToolBase.lswordMaterial, 0, -2.4f,new Item.Properties().tab(TalismanMod.CREATIVETABNAME)).setRegistryName(location("large_sword"))
                );
        TalismanMod.LOGGER.info("items registered");
    }

    @SubscribeEvent
    public static void registerBlocks(final RegistryEvent.Register<Block> event) {
        event.getRegistry().registerAll
                (
                        BlockList.tutorial_block = new Block(AbstractBlock.Properties.of(Material.METAL).strength(5.0f, 2.0f).harvestLevel(3).harvestTool(ToolType.AXE).requiresCorrectToolForDrops().sound(SoundType.METAL)).setRegistryName(location("tutorial_block"))
                );
        TalismanMod.LOGGER.info("blocks registered");
    }

}
