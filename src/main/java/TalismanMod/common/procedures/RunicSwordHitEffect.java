package TalismanMod.common.procedures;


import TalismanMod.TalismanMod;
import TalismanMod.common.PotionEffectBase;
import net.minecraft.entity.Entity;
import net.minecraft.entity.LivingEntity;
import net.minecraft.potion.EffectInstance;
import net.minecraft.potion.Effects;
import net.minecraftforge.common.MinecraftForge;
import java.util.Map;

@PotionEffectBase.ModElement.Tag
public class RunicSwordHitEffect extends PotionEffectBase.ModElement {
    public RunicSwordHitEffect(PotionEffectBase instance) {
        super(instance, 1);
        MinecraftForge.EVENT_BUS.register(this);
    }

    public static void executeProcedure(Map<String, Object> dependencies) {
        if (dependencies.get("sourceEntity") == null) {
            if (!dependencies.containsKey("sourceEntity"))
                TalismanMod.LOGGER.warn("Failed to load dependency sourceEntity for procedure RunicSwordHitEffect!");
            return;
        }
        Entity sourceEntity = (Entity) dependencies.get("sourceEntity");
        if (sourceEntity instanceof LivingEntity)
            ((LivingEntity) sourceEntity).addEffect(new EffectInstance(Effects.REGENERATION, (int) 30, (int) 1));
    }
}
