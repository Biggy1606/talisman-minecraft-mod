package TalismanMod.common.entities;

import net.minecraft.entity.EntityType;
import net.minecraft.item.Item;
import net.minecraft.item.ItemGroup;
import net.minecraft.item.SpawnEggItem;

public class EntitiesList
{
    public static EntityType<?> T_ENTITY;

    public static void registerEntitySpwanEgg(EntityType<?> type, int color1, int color2)
    {
        SpawnEggItem item = new SpawnEggItem(type, color1, color2, new Item.Properties().tab(ItemGroup.TAB_MISC));
    }
}
