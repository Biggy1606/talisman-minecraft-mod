package TalismanMod.common.items;

import TalismanMod.common.items.ItemList;
import net.minecraft.item.IItemTier;
import net.minecraft.item.Item;
import net.minecraft.item.crafting.Ingredient;

public enum ItemToolBase implements IItemTier
{
    runicMaterial(7.0f, 1.6f, 1562, 3, 25, ItemList.runic_sword),
    swordMaterial(7.0f, 1.6f, 251, 3, 25, ItemList.sword),
    lswordMaterial(7.0f, 1.6f, 251, 3, 25, ItemList.sword);

    private float attackDamage, efficiency;
    private int durability, harvestLevel, enchantability;
    private Item repairMaterial;

    private ItemToolBase(float attackDamage, float efficiency, int durability, int harvestLevel, int enchantability, Item repairMaterial)
    {
    this.attackDamage = attackDamage;
    this.efficiency = efficiency;
    this.durability = durability;
    this.harvestLevel = harvestLevel;
    this.enchantability = enchantability;
    this.repairMaterial = repairMaterial;
    }

    @Override
    public int getUses() {
        return this.durability;
    }

    @Override
    public float getSpeed() {
        return this.efficiency;
    }

    @Override
    public float getAttackDamageBonus() {
        return this.attackDamage;
    }

    @Override
    public int getLevel() {
        return this.harvestLevel;
    }

    @Override
    public int getEnchantmentValue() {
        return this.enchantability;
    }

    @Override
    public Ingredient getRepairIngredient() {
        return Ingredient.of(this.repairMaterial);
    }
}
