package TalismanMod.common;

import net.minecraft.block.Block;
import net.minecraft.enchantment.Enchantment;
import net.minecraft.entity.EntityType;
import net.minecraft.item.Item;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.SoundEvent;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.api.distmarker.OnlyIn;
import net.minecraftforge.event.RegistryEvent;
import net.minecraftforge.fml.ModList;
import net.minecraftforge.fml.event.lifecycle.FMLClientSetupEvent;
import net.minecraftforge.fml.event.lifecycle.FMLCommonSetupEvent;
import net.minecraftforge.fml.event.server.FMLServerStartingEvent;
import net.minecraftforge.forgespi.language.ModFileScanData;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.util.*;
import java.util.function.Supplier;

public class PotionEffectBase {
    public final List<ModElement> elements = new ArrayList<>();
    public final List<Supplier<Block>> blocks = new ArrayList<>();
    public final List<Supplier<Item>> items = new ArrayList<>();
    public final List<Supplier<EntityType<?>>> entities = new ArrayList<>();
    public final List<Supplier<Enchantment>> enchantments = new ArrayList<>();
    public static Map<ResourceLocation, SoundEvent> sounds = new HashMap<>();

    public PotionEffectBase() {
        try {
            ModFileScanData modFileInfo = ModList.get().getModFileById("talisman").getFile().getScanResult();
            Set<ModFileScanData.AnnotationData> annotations = modFileInfo.getAnnotations();
            for (ModFileScanData.AnnotationData annotationData : annotations) {
                if (annotationData.getAnnotationType().getClassName().equals(ModElement.Tag.class.getName())) {
                    Class<?> clazz = Class.forName(annotationData.getClassType().getClassName());
                    if (clazz.getSuperclass() == PotionEffectBase.ModElement.class)
                        elements.add((PotionEffectBase.ModElement) clazz.getConstructor(this.getClass()).newInstance(this));
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        Collections.sort(elements);
        elements.forEach(PotionEffectBase.ModElement::initElements);
    }

    public void registerSounds(RegistryEvent.Register<net.minecraft.util.SoundEvent> event) {
        for (Map.Entry<ResourceLocation, net.minecraft.util.SoundEvent> sound : sounds.entrySet())
            event.getRegistry().register(sound.getValue().setRegistryName(sound.getKey()));
    }

    public List<ModElement> getElements() {
        return elements;
    }

    public List<Supplier<Block>> getBlocks() {
        return blocks;
    }

    public List<Supplier<Item>> getItems() {
        return items;
    }

    public List<Supplier<EntityType<?>>> getEntities() {
        return entities;
    }

    public List<Supplier<Enchantment>> getEnchantments() {
        return enchantments;
    }


    public static class ModElement implements Comparable<ModElement> {
        @Retention(RetentionPolicy.RUNTIME)
        public @interface Tag {
        }

        protected final PotionEffectBase elements;
        protected final int sortid;

        public ModElement(PotionEffectBase elements, int sortid) {
            this.elements = elements;
            this.sortid = sortid;
        }

        public void initElements() {
        }

        public void init(FMLCommonSetupEvent event) {
        }

        public void serverLoad(FMLServerStartingEvent event) {
        }

        @OnlyIn(Dist.CLIENT)
        public void clientLoad(FMLClientSetupEvent event) {
        }

        @Override
        public int compareTo(ModElement other) {
            return this.sortid - other.sortid;
        }
    }
}
