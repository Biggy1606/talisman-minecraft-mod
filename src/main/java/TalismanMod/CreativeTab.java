package TalismanMod;

import TalismanMod.common.items.ItemList;
import net.minecraft.item.ItemGroup;
import net.minecraft.item.ItemStack;

public class CreativeTab extends ItemGroup
{
    public CreativeTab()
    {
        super("talisman");
    }
    @Override
    public ItemStack makeIcon()
    {
        return new ItemStack(ItemList.coin);
    }
}
